import React, { Component } from 'react';
import Aux from '../../hoc/Auxillary';
import classes from './Layout.css';

import ToolBar from '../Navigation/ToolBar/ToolBar';
import SideDrawer from '../Navigation/SideDrawer/SideDrawer';

class layout extends Component{
    
    state={
        showSideDrawer: false
    }
    
    sideDrawerCloseHandler = () =>{
        this.setState({showSideDrawer: false})
    }
    
    
    sideDrawerToggleHandler = () =>{
        this.setState((prevState)=>{
         return {showSideDrawer: !prevState.showSideDrawer};
        })
    }
    render(){
        return(
    <Aux>
        <div>
            <ToolBar toggleMenu={this.sideDrawerToggleHandler}/>
            <SideDrawer open={this.state.showSideDrawer} closed={this.sideDrawerCloseHandler}/>
        </div>
        <main className={classes.Content}>
            {this.props.children}
        </main>
    </Aux>
    )
}
}

export default layout;