import React,{Component} from 'react';
import Aux from '../../../hoc/Auxillary';
import Button from '../../UI/Button/Button';

class OrderSummary extends Component {
    
    componentDidUpdate(){
        console.log("Order Summary will upadte")
    }

    render(){
    const ingredientSummary = Object.keys(this.props.ingredients)
                                .map(igKey=>{
                                return <li key={igKey}>
                                    <span style={{textTransform: 'capitalize'}}>
                                        {igKey}</span> : {this.props.ingredients[igKey]}
                                    </li>
                                });
    return(
        <Aux>
            <h3>Your Order</h3>
            <ul>
                {ingredientSummary}
            </ul>
            <h4><strong>Total Price: {this.props.price.toFixed(2)} </strong></h4>
            <p>Continue to Checkout?</p>
            <Button btnType="Danger" clicked={this.props.onCancel}>Cancel</Button>
            <Button btnType="Success" clicked={this.props.onContinue}>Continue</Button>

        </Aux>
    )
}
}

export default OrderSummary;