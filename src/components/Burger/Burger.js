import React from 'react';

import classes from './Burger.css';
import BurgerIngredient from './BurgerIngredients/BurgerIngredients';


const burger = (props) =>{
    let ingredients = Object.keys(props.ingredients)
        .map((igkey) =>{
            return [...Array(props.ingredients[igkey])].map((_, i) =>{
               return <BurgerIngredient key={igkey + i} type={igkey} />
            }) // [Has array of 2 elements]
        })
        .reduce((arr, el) =>{
            return arr.concat(el)
        }, [])
        if (ingredients.length === 0){
            ingredients = <p> Please Start adding ingredients</p>
        }
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top"/>
            {ingredients}
            <BurgerIngredient type="bread-bottom"/>
        </div>
    );
}

export default burger;